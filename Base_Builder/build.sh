# Set Inital Vars
export containerName=Parrot
export imageTag=parrot
export distroName=install

# Build the base image
docker build --file $containerName.dockerfile --tag $imageTag .
docker run --name $containerName $imageTag
docker export --output=build/$distroName.tar.gz $containerName

# Zip Files
zip build/output.zip build/*